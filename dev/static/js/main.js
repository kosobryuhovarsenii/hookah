new WOW().init();

$(document).ready(function () {
  $(".header-menu a").on("click", function (e) {
    e.preventDefault();
    var elementClick = $(this).attr("href");
    var destination = $(elementClick).offset().top;
    $("html, body").animate(
      {
        scrollTop: destination - $(".header").height(),
      },
      500,
      "linear"
    );
    $(".header-info").toggleClass("active");
    $(".header-menu").toggleClass("flex");
  });

  $(".banner .button").on("click", function (e) {
    e.preventDefault();
    var elementClick = $(this).attr("href");
    var destination = $(elementClick).offset().top;
    $("html, body").animate(
      {
        scrollTop: destination - $(".header").height(),
      },
      500,
      "linear"
    );
    $(".header-info").toggleClass("active");
    $(".header-menu").toggleClass("flex");
  });

  $(".header-logo").on("click", function (e) {
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop: 0,
      },
      500,
      "linear"
    );
  });

  $(".header-menu__button").click(function () {
    $(".header-info").toggleClass("active");
    $(".header-menu").toggleClass("flex");
  });

  $(".faq-accordion__item").on("click", function () {
    $(this).find("span").toggleClass("active");
    $(this).find(".faq-accordion__item-content").slideToggle("slow");
  });

  $(".service-card .button").on("click", function () {
    let nameCard = $(this).parent().find(".service-card__title").text();
    $(".modal-title").text(nameCard);

    $(".modal").fadeIn(300);
    $(".modal-bg").fadeIn(300);
  });

  $("body").on("click", ".modal-btn", function (e) {
    e.preventDefault();
    $(".modal").fadeOut(300);
    $(".modal-bg").fadeOut(300);
  });
  $("body").on("click", ".modal-bg", function (e) {
    e.preventDefault();
    $(".modal").fadeOut(300);
    $(".modal-bg").fadeOut(300);
  });

  // маска
  $("input[type=tel]").mask("+7 (999) 999-9999");

  // отправка формы
  $(function () {
    $("#modal-form").on("submit", function (e) {
      e.preventDefault();
      $.ajax({
        type: "POST",
        url: "static/mailer/mail.php",
        data: $(this).serialize(),
        success: function () {

          console.log($("#modal-form").serialize())
          $(".modal").fadeOut(300);

          $(".modal-success__title").html(
            "Спасибо за заявку, скоро мы вам перезвоним!"
          );
          $(".modal-success").fadeIn(300);

          setTimeout(function () {
            $(".modal-success").fadeOut(300);
            $(".modal-bg").fadeOut(300);
          }, 3000);

          $("input").val("");
        },
        error: function (jqXHR, textStatus) {
          console.log(jqXHR + ": " + textStatus);
        },
      });
      return false;
    });
  });

  $(function () {
    $("#contacts-form").on("submit", function (e) {
      e.preventDefault();
      $.ajax({
        type: "POST",
        url: "static/mailer/mail.php",
        data: $(this).serialize(),
        success: function () {
          $(".modal-success__title").html(
            "Спасибо за заявку, скоро мы c вам перезвоним!"
          );
          $(".modal-success").fadeIn(300);
          $(".modal-bg").fadeIn(300);

          setTimeout(function () {
            $(".modal-success").fadeOut(300);
            $(".modal-bg").fadeOut(300);
          }, 3000);

          $("input").val("");
          $("textarea").val("");
        },
        error: function (jqXHR, textStatus) {
          console.log(jqXHR + ": " + textStatus);
        },
      });
      return false;
    });
  });
});
